#pragma once
#include "ofMain.h"

class MiniBee {
   public:
    MiniBee();
    ~MiniBee() = default;

    void update();
    inline auto getX() const { return m_current[0]; };
    inline auto getY() const { return m_current[1]; };
    inline auto getZ() const { return m_current[2]; };
    inline auto getRssi() const { return m_rssi; };
    inline auto getDelta() const { return m_delta; };
    inline auto getAvgDelta() const { return m_runningAvgDelta; };
    inline auto isConnected() const { return m_isConnected; };

    void setX(float in);
    void setY(float in);
    void setZ(float in);
    void setRssi(float in);
    void calcDelta();
    void checkConnection();
    void calcRunningAvgDelta();

   private:
    glm::vec3 m_prev = {0.0, 0.0, 0.0};
    glm::vec3 m_current = {0.0, 0.0, 0.0};
    float m_delta = 0.0;
    float m_runningAvgDelta = 0.0;
    deque<float> m_deltas;
    size_t m_windowSize = 50;
    bool m_isConnected = false;
    float m_rssi = 0.0;
};
