#include "MiniBee.h"

MiniBee::MiniBee() {
    for (size_t i = 0; i < m_windowSize; i++) {
        m_deltas.push_back(0.0);
    }
}

void MiniBee::update() {
    calcDelta();
    calcRunningAvgDelta();
    checkConnection();
}

void MiniBee::setX(float in) {
    const float min = 0.47020;
    const float max = 0.534;
    m_current[0] = ofMap(in, min, max, 0.0, 1.0, true);
}

void MiniBee::setY(float in) {
    const float min = 0.469;
    const float max = 0.5322;
    m_current[1] = ofMap(in, min, max, 0.0, 1.0, true);
}

void MiniBee::setZ(float in) {
    const float min = 0.469;
    const float max = 0.650;
    m_current[2] = ofMap(in, min, max, 0.0, 1.0, true);
}

void MiniBee::calcDelta() {
    const float numDimensions = 3;
    const ofVec3f delta = abs(m_current - m_prev) / numDimensions;
    m_delta = delta[0] + delta[1] + delta[2];
    m_prev = m_current;
}

void MiniBee::setRssi(float in) {
    const float min = 29;
    const float max = 71;
    m_rssi = ofMap(in, min, max, 0.0, 1.0, true);
}

void MiniBee::checkConnection() {
    m_isConnected = m_runningAvgDelta != 0.0;
}

void MiniBee::calcRunningAvgDelta() {
    m_deltas.push_back(m_delta);
    m_deltas.pop_front();
    for (auto d : m_deltas) {
        ofLog() << ofToString(d) << " ";
    }
    ofLog() << endl;
    // m_runningAvgDelta += (m_delta - m_deltas[0]) / m_windowSize;
    m_runningAvgDelta =
        (std::accumulate(m_deltas.begin(), m_deltas.end(), 0.0)) /
        m_deltas.size();
}
