#pragma once

#include "ofMain.h"
#include "globalvars.h"
#include "ofxOsc.h"
#include "sensor/MiniBee.h"

class MBDeltaTrig {
public:
    MBDeltaTrig();
    MBDeltaTrig(MBDeltaTrig &&) = default;
    MBDeltaTrig(const MBDeltaTrig &) = default;
    MBDeltaTrig &operator=(MBDeltaTrig &&) = default;
    MBDeltaTrig &operator=(const MBDeltaTrig &) = default;
    ~MBDeltaTrig();
    void setup();
    void update();
    inline auto getThreshold() const { return m_threshold; };
    inline auto getDeadTime() const { return m_deadTime; };
    inline auto getRemoteHost() const { return m_remoteHost; };
    inline auto getRemotePort() const { return m_remotePort; };
    void setMiniBeeId(int in){ m_mbID = in; };
    void setThreshold(float in){ m_threshold = in; };
    void setDeadTime(float in){ m_deadTime = in; };
    void setRemoteHost(string in){ m_remoteHost = std::move(in); };
    void setRemotePort(int in){ m_remotePort = in; };
    void setActionId(int in){ m_actionID = in; };

private:
    int m_mbID;
    int m_actionID;
    float m_threshold;
    float m_deadTime;
    string m_remoteHost;
    int m_remotePort;
    float m_t0;
};
