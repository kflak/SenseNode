#include "MBDeltaTrig.h"

MBDeltaTrig::MBDeltaTrig( ) = default;

MBDeltaTrig::~MBDeltaTrig() = default;

void MBDeltaTrig::setup() {
    m_t0 = ofGetElapsedTimef();
}
void MBDeltaTrig::update() {
    const float delta = mbData[m_mbID].getDelta();
    const float timeSinceLastTrigger = ofGetElapsedTimef() - m_t0;
    const string oscSenderName = m_remoteHost + ":" + ofToString(m_remotePort);
    if (delta > m_threshold && timeSinceLastTrigger > m_deadTime) {
        ofxOscMessage msg;
        msg.setAddress("/senseNode/mbDeltaTrig/" + ofToString(m_actionID));
        msg.addIntArg(m_mbID);
        msg.addFloatArg(delta);
        oscSender[oscSenderName].sendMessage(msg);
        m_t0 = ofGetElapsedTimef();
    }
}
