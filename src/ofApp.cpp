#include "ofApp.h"
#include "util/stringhasher.h"

//--------------------------------------------------------------
void SenseNode::setup() {
    const int frameRate = 20;
    ofSetFrameRate(frameRate);
    m_receiver.setup(receivePort);
    loadSettings();
}

//--------------------------------------------------------------
void SenseNode::update() {

    handleOsc();

    for (auto &mb : mbData) {
        mb.second.update();
    };

    for (auto &deltaTrigger : mbDeltaTrig) {
        deltaTrigger.second.update();
    };
}

//--------------------------------------------------------------
void SenseNode::draw() {
    ofBackground(0);
    ofSetColor(255);
    drawMBValues();
}

//--------------------------------------------------------------
void SenseNode::keyPressed(int key) {
    switch (key) {
        case 'q': {
            ofExit(0);
            break;
        }
        default:

            break;
    }
}

//--------------------------------------------------------------
void SenseNode::keyReleased(int key) {}

//--------------------------------------------------------------
void SenseNode::mouseMoved(int x, int y) {}

//--------------------------------------------------------------
void SenseNode::mouseDragged(int x, int y, int button) {}

//--------------------------------------------------------------
void SenseNode::mousePressed(int x, int y, int button) {}

//--------------------------------------------------------------
void SenseNode::mouseReleased(int x, int y, int button) {}

//--------------------------------------------------------------
void SenseNode::mouseEntered(int x, int y) {}

//--------------------------------------------------------------
void SenseNode::mouseExited(int x, int y) {}

//--------------------------------------------------------------
void SenseNode::windowResized(int w, int h) {}

//--------------------------------------------------------------
void SenseNode::gotMessage(ofMessage msg) {}

//--------------------------------------------------------------
void SenseNode::dragEvent(ofDragInfo dragInfo) {}

void SenseNode::handleOsc() {

    while (m_receiver.hasWaitingMessages()) {
        ofxOscMessage message;
        m_receiver.getNextMessage(message);

        const string address = message.getAddress();

        switch (hash_djb2a(address)) {
            case "/minibee/rssi"_sh: {
                const int id = message.getArgAsInt(0);
                const int rssi = message.getArgAsInt(1);
                if (mbData.find(id) != mbData.end()) {
                    mbData[id].setRssi(rssi);
                } else {
                    mbData[id] = MiniBee();
                    mbData[id].setRssi(rssi);
                };
                break;
            }
            case "/minibee/data"_sh: {
                const int id = message.getArgAsInt(0);
                const float x = message.getArgAsFloat(1);
                const float y = message.getArgAsFloat(2);
                const float z = message.getArgAsFloat(3);
                if (mbData.find(id) != mbData.end()) {
                    mbData[id].setX(x);
                    mbData[id].setY(y);
                    mbData[id].setZ(z);
                } else {
                    mbData[id] = MiniBee();
                    mbData[id].setX(x);
                    mbData[id].setY(y);
                    mbData[id].setZ(z);
                };
                break;
            }
            case "/action/mbDeltaTrig/create"_sh: {
                const int id = message.getArgAsInt(0);
                const int mbId = message.getArgAsInt(1);
                const float threshold = message.getArgAsFloat(2);
                const float deadTime = message.getArgAsFloat(3);
                string remoteHost = message.getRemoteHost();
                const int remotePort = message.getRemotePort();
                if (message.getNumArgs() == 4) {
                    mbDeltaTrig[id] = MBDeltaTrig();
                    mbDeltaTrig[id].setMiniBeeId(mbId);
                    mbDeltaTrig[id].setThreshold(threshold);
                    mbDeltaTrig[id].setDeadTime(deadTime);
                    mbDeltaTrig[id].setRemoteHost(remoteHost);
                    mbDeltaTrig[id].setRemotePort(remotePort);
                    mbDeltaTrig[id].setActionId(id);
                    mbDeltaTrig[id].setup();
                } else {
                    ofLogWarning() << "Wrong number of arguments";
                }
                break;
            }
            case "/connect"_sh: {
                //TODO: make it possible to connect to other host/port than sender
                string host = message.getRemoteHost();
                int port = message.getRemotePort();
                string name = host + ":" + ofToString(port);
                if (oscSender.find(name) == oscSender.end()) {
                    oscSender[name].clear();
                }
                oscSender[name] = ofxOscSender();
                oscSender[name].setup(host, port);
                ofLogNotice() << "Connected to " << oscSender[name].getHost()
                              << ":" << oscSender[name].getPort();
                ofxOscMessage msg;
                msg.setAddress("/senseNode");
                msg.addStringArg("Connected to SenseNode!");
                oscSender[name].sendMessage(msg);
                break;
            }
            default: {
                ofLogWarning() << "OSC command not assigned";
            }
        }
    }
}

void SenseNode::drawMBValues() {
    const std::array<float, 8> xOffset = {20, 80, 140, 200, 260, 320, 420, 580};
    const float defaultYOffset = 30;
    float yOffset = defaultYOffset;
    const float yOffsetInc = defaultYOffset;
    for (auto &mb : mbData) {
        if (mb.second.isConnected()) {
            ofSetColor(255);
        } else {
            ofSetColor(255, 0, 0);
        }
        const string id = ofToString(mb.first);
        const string x = ofToString(mb.second.getX()).substr(0, 4);
        const string y = ofToString(mb.second.getY()).substr(0, 4);
        const string z = ofToString(mb.second.getZ()).substr(0, 4);
        const string delta = ofToString(mb.second.getDelta()).substr(0, 4);
        const string rssi = ofToString(mb.second.getRssi()).substr(0, 4);
        const string runningAvgDelta =
            ofToString(mb.second.getAvgDelta()).substr(0, 4);
        ofDrawBitmapString("id:" + id, xOffset[0], yOffset);
        ofDrawBitmapString("x:" + x, xOffset[1], yOffset);
        ofDrawBitmapString("y:" + y, xOffset[2], yOffset);
        ofDrawBitmapString("z:" + z, xOffset[3], yOffset);
        ofDrawBitmapString("dt:" + delta, xOffset[4], yOffset);
        ofDrawBitmapString("rssi:" + rssi, xOffset[5], yOffset);
        ofDrawBitmapString("runningAvgDt: " + runningAvgDelta, xOffset[6],
                           yOffset);
        if (mb.second.isConnected()) {
            ofDrawBitmapString("Connected", xOffset[7], yOffset);
        } else {
            ofDrawBitmapString("NOT CONNECTED!", xOffset[7], yOffset);
        };
        yOffset += yOffsetInc;
    }
}

void SenseNode::loadSettings() {

    string settingsFilePath =
        std::filesystem::current_path().string() + "/senseNode.xml";
    ofFile settingsFile(settingsFilePath);
    if (settingsFile.exists()) {
        settings.load(settingsFilePath);

        ofLogNotice() << "Settings loaded from senseNode.xml";
    } else {
        settings.load("settings.xml");
        ofLogNotice() << "Settings loaded from settings.xml";
    }

    // testing purposes only
    int blinkRate = settings.getValue("settings:blinkRate", 100);
    ofLogNotice() << "Blink rate: " << blinkRate;
}
