#pragma once
#include "action/MBDeltaTrig.h"
#include "MiniBee.h"
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxXmlSettings.h"

class MBDeltaTrig;

extern std::map<int, MiniBee> mbData;
extern std::map<int, MBDeltaTrig> mbDeltaTrig;
extern std::map<string, ofxOscSender> oscSender;
extern ofxXmlSettings settings;
