#include "globalvars.h"

std::map<int, MiniBee> mbData;
std::map<int, MBDeltaTrig> mbDeltaTrig;
std::map<string, ofxOscSender> oscSender;
ofxXmlSettings settings;
