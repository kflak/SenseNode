#pragma once

#include "action/MBDeltaTrig.h"
#include "globalvars.h"
#include "ofMain.h"
#include "ofxOsc.h"
#include "sensor/MiniBee.h"

class SenseNode : public ofBaseApp {

   public:
    void setup() override;
    void update() override;
    void draw() override;
    void drawMBValues();
    // void checkAlive(mbData &mb);

    void keyPressed(int key) override;
    void keyReleased(int key) override;
    void mouseMoved(int x, int y) override;
    void mouseDragged(int x, int y, int button) override;
    void mousePressed(int x, int y, int button) override;
    void mouseReleased(int x, int y, int button) override;
    void mouseEntered(int x, int y) override;
    void mouseExited(int x, int y) override;
    void windowResized(int w, int h) override;
    void dragEvent(ofDragInfo dragInfo) override;
    void gotMessage(ofMessage msg) override;

    const int defaultReceivePort = 12345;
    int receivePort = defaultReceivePort;
    void handleOsc();
    // void registerAction(const string& type, int id, const string& remoteHost, int remotePort);
   private:
    ofxOscReceiver m_receiver;
    void loadSettings();
};
