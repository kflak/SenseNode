# README

<!--toc:start-->
- [README](#readme)
  - [OSC reference](#osc-reference)
    - [MBDeltaTrigger](#mbdeltatrigger)
<!--toc:end-->

## OSC reference

### Port

Default port is 12345. Currently this is hard-coded. TODO: make it
configurable via command line arguments and/or config file from cwd.

### MBDeltaTrigger

OSC address: `/action/mbDeltaTrig`

Actions:

```supercollider
/action/mbDeltaTrig/create, <int actionID>, <int mbID>, <float threshold>, <float deadTime>
```
Example:

```supercollider
'/action/mbDeltaTrig/create', 1, 9, 0.1, 0.5
```

This creates a deltatrigger with the id 1 for minibee 9, with a
threshold of 0.1, and a deadTime of 0.5 seconds before it can be re-triggered. The deltaTrigger will send an osc message to 
```
"/senseNode/mbDeltaTrig/<actionID>"
``` 
with the args: `<mbID>, <deltaValue>`

